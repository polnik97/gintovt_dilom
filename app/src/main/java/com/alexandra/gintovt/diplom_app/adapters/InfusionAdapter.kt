package com.alexandra.gintovt.diplom_app.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.alexandra.gintovt.diplom_app.InfusShow
import com.alexandra.gintovt.diplom_app.R
import com.alexandra.gintovt.diplom_app.dto.InfusionDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import com.alexandra.gintovt.diplom_app.utils.formatToViewedTime
import java.time.LocalDateTime

class InfusionAdapter(infusions: List<InfusionDto>, context: Context?, activity: FragmentActivity?) :
    RecyclerView.Adapter<InfusionAdapter.ViewHolder>() {

    val dataProcessor = DataProcessor.instance
    private val infusionsList = dataProcessor.getInfusions().toMutableList()
    private val contextIner = context
    private val avtivityIner = activity

    class ViewHolder(view: View, activity: FragmentActivity?) : RecyclerView.ViewHolder(view) {

        private val patientName: TextView = view.findViewById<TextView>(R.id.dragName)
        private val drag: TextView = view.findViewById<TextView>(R.id.darg)
        private val startTime: TextView = view.findViewById<EditText>(R.id.startDate)
        private val endTime: TextView = view.findViewById<EditText>(R.id.endDate)
        private val act = activity

        fun bind(infusion: InfusionDto, context: Context?, holder: ViewHolder) {
            patientName.text = infusion.patientName
            drag.text = infusion.dragName
            startTime.text = infusion.startDate?.formatToViewedTime() ?: ""
            endTime.text = infusion.endDate?.formatToViewedTime() ?: ""
            if(infusion.endDate!!.isBefore(LocalDateTime.now())){
                holder.itemView.setBackgroundColor(Color.GRAY)
            }
            else{
                holder.itemView.setBackgroundColor(Color.parseColor("#ADDC77"))
            }
            itemView.setOnClickListener{
                val intent = Intent(act, InfusShow::class.java).apply {
                    putExtra("infusionId", infusion.id)
                }
                context?.startActivity(intent)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(contextIner)
        return ViewHolder(inflater.inflate(R.layout.infusion_element, parent, false), avtivityIner)
    }

    override fun getItemCount(): Int {
        return infusionsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val infusion = infusionsList[position]
        holder.bind(infusion, contextIner, holder)
    }

    fun updateAdapter(list: List<InfusionDto>){
        infusionsList.clear()
        infusionsList.addAll(list)
        notifyDataSetChanged()
    }
}