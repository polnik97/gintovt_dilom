package com.alexandra.gintovt.diplom_app.proccessors

import com.alexandra.gintovt.diplom_app.dto.*

class DataProcessor
private constructor(){
    init {
    }

    private val infusionsList: MutableList<InfusionDto> = infusions.toMutableList()
    val currentInfusionsIds: MutableList<Int> = mutableListOf()
    var currentUser: DoctorDto? = null

    private object Holder{
        val INSTANCE = DataProcessor()
    }

    companion object{
        val instance: DataProcessor by lazy{ Holder.INSTANCE}
    }


    fun getInfusions(): List<InfusionDto> {
        return infusionsList
    }

    fun getInfusionById(id: Int): InfusionDto {
        return infusionsList.first{ it.id == id}
    }

    fun getDoctors(): List<DoctorDto> {
        return doctors
    }

    fun getDoctorsById(id: Int): DoctorDto {
        return doctors.first{ it.id == id}
    }

    fun getDrags(): List<DragDto> {
        return drags
    }

    fun getDragsById(id: Int): DragDto {
        return drags.first{ it.id == id}
    }

    fun getPatient(): List<PatientDto> {
        return patients
    }

    fun getPatientById(id: Int): PatientDto {
        return patients.first{ it.id == id}
    }

    fun getSensors(): List<SensorDto> {
        return sensors
    }

    fun getSensorById(id: Int): SensorDto {
        return sensors.first{ it.id == id}
    }

    fun saveInfusion(infusionDto: InfusionDto){
        infusionsList.add(infusionDto)
    }
}