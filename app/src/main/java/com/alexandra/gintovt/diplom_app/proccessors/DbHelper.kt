package com.alexandra.gintovt.diplom_app.proccessors

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.alexandra.gintovt.diplom_app.dto.*

val DATABAS_NAME = "GINTOVT_DB"

val ID = "ID"
val DOCTOR_NAME = "DOCTOR_NAME"
val DOCTRO_OCCUPATION = "DOCTOR_OCCUPATION"

val DRAG_NAME = "DRAG_NAME"
val DRAG_VALUE = "DRAG_VALUE"

val PATIENT_NAME = "PATIENT_NAME"
val PATIETN_ROOM = "PATIENT_ROOM"

val SENSOR_NAME = "SENSOR_NAME"
val SENSOR_SPEED = "SENSOR_SPEED"

val PATIETN_ID = "PATIETN_ID"
val SENSOR_ID = "SENSOR_ID"
val START_DATE = "START_DATE"
val END_DATE =  "END_DATE"
val CREATOR_NAME = "CREATOR_NAME"
val LAST_CHANGE_NAME = "LAST_CHANGE_NAME"
val DOCTOR_TRACED_ID = "DOCTOR_TRACED_ID"

val DOCTORS_TABLE = "Doctors"
val DRAGS_TABLE = "Drags"
val PATIENTS_TABLE = "Patients"
val SENSORS_TABLE = "Sensors"
val INFUSIONS_TABLE = "InfusionsList"
val TRAKED_INFUSIONS_TABLE = "InfusionsTaking"

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABAS_NAME, null, 1)  {

    override fun onCreate(db: SQLiteDatabase?) {
        val createDoctorTable = "CREATE TABLE $DOCTORS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$DOCTOR_NAME VARCHAR(256)," +
                "$DOCTRO_OCCUPATION VARCHAR(256))"

        val createDragTable = "CREATE TABLE $DRAGS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$DRAG_NAME VARCHAR(256)," +
                "$DRAG_VALUE INTEGER)"

        val createPatientTable = "CREATE TABLE $PATIENTS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$PATIENT_NAME VARCHAR(256)," +
                "$PATIETN_ROOM VARCHAR(256))"

        val createSensorTable = "CREATE TABLE $SENSORS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$SENSOR_NAME VARCHAR(256)," +
                "$SENSOR_SPEED REAL)"

        val createInfusionsTable = "CREATE TABLE $INFUSIONS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$PATIETN_ID INTEGER," +
                "$PATIENT_NAME VARCHAR(256)," +
                "$PATIETN_ROOM VARCHAR(256)," +
                "$DRAG_NAME VARCHAR(256)," +
                "$DRAG_VALUE INTEGER," +
                "$SENSOR_ID INTEGER," +
                "$SENSOR_NAME VARCHAR(256)," +
                "$SENSOR_SPEED REAL" +
                "$START_DATE VARCHAR(256)," +
                "$END_DATE VARCHAR(256)," +
                "$CREATOR_NAME VARCHAR(256)," +
                "$LAST_CHANGE_NAME VARCHAR(256))"

        val createTakingTable = "CREATE TABLE $TRAKED_INFUSIONS_TABLE (" +
                "$ID INSERT PRIMARY KEY AUTOINCREMENT," +
                "$PATIETN_ID INTEGER," +
                "$PATIENT_NAME VARCHAR(256)," +
                "$PATIETN_ROOM VARCHAR(256)," +
                "$DRAG_NAME VARCHAR(256)," +
                "$DRAG_VALUE INTEGER," +
                "$SENSOR_ID INTEGER," +
                "$SENSOR_NAME VARCHAR(256)," +
                "$SENSOR_SPEED REAL" +
                "$START_DATE VARCHAR(256)," +
                "$END_DATE VARCHAR(256)," +
                "$CREATOR_NAME VARCHAR(256)," +
                "$LAST_CHANGE_NAME VARCHAR(256)," +
                "$DOCTOR_TRACED_ID INTEGER)"

        db?.execSQL(createDoctorTable)
        db?.execSQL(createDragTable)
        db?.execSQL(createInfusionsTable)
        db?.execSQL(createSensorTable)
        db?.execSQL(createPatientTable)
        db?.execSQL(createTakingTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    fun insertInfusion(infusionDto: InfusionDto){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(PATIETN_ID, infusionDto.patientId)
        cv.put(PATIENT_NAME, infusionDto.patientName)
        cv.put(PATIETN_ROOM, infusionDto.patientRoomNumber)
        cv.put(DRAG_NAME, infusionDto.dragName)
        cv.put(DRAG_VALUE, infusionDto.dragValue)
        cv.put(SENSOR_ID, infusionDto.sensorId)
        cv.put(SENSOR_NAME, infusionDto.sensorName)
        cv.put(SENSOR_SPEED, infusionDto.sensorSpeed)
        cv.put(START_DATE, infusionDto.startDate.toString())
        cv.put(END_DATE, infusionDto.endDate.toString())
        cv.put(CREATOR_NAME, infusionDto.creatorName)
        cv.put(LAST_CHANGE_NAME, infusionDto.lastChangerName)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }

    fun insertTakingInfusion(infusionDto: InfusionDto, doctorId: Int){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(PATIETN_ID, infusionDto.patientId)
        cv.put(PATIENT_NAME, infusionDto.patientName)
        cv.put(PATIETN_ROOM, infusionDto.patientRoomNumber)
        cv.put(DRAG_NAME, infusionDto.dragName)
        cv.put(DRAG_VALUE, infusionDto.dragValue)
        cv.put(SENSOR_ID, infusionDto.sensorId)
        cv.put(SENSOR_NAME, infusionDto.sensorName)
        cv.put(SENSOR_SPEED, infusionDto.sensorSpeed)
        cv.put(START_DATE, infusionDto.startDate.toString())
        cv.put(END_DATE, infusionDto.endDate.toString())
        cv.put(CREATOR_NAME, infusionDto.creatorName)
        cv.put(LAST_CHANGE_NAME, infusionDto.lastChangerName)
        cv.put(DOCTOR_TRACED_ID, doctorId)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }

    fun insertDoctors(doctorDto: DoctorDto){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(DOCTOR_NAME, doctorDto.name)
        cv.put(DOCTRO_OCCUPATION, doctorDto.occupation)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }

    fun insertDrags(dragsDto: DragDto){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(DRAG_NAME, dragsDto.name)
        cv.put(DRAG_VALUE, dragsDto.value)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }

    fun insertSensors(sensorDto: SensorDto){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(SENSOR_NAME, sensorDto.name)
        cv.put(SENSOR_SPEED, sensorDto.speed)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }

    fun insertPatient(patientDto: PatientDto){
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(PATIENT_NAME, patientDto.patientName)
        cv.put(PATIETN_ROOM, patientDto.patientRoom)
        val result = db.insert(INFUSIONS_TABLE, null, cv)
    }
}