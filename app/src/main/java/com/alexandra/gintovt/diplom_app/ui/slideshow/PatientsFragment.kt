package com.alexandra.gintovt.diplom_app.ui.slideshow

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SimpleAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.alexandra.gintovt.diplom_app.PatientShow
import com.alexandra.gintovt.diplom_app.R
import com.alexandra.gintovt.diplom_app.dto.PatientDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import kotlinx.android.synthetic.main.fragment_patients.*

class PatientsFragment : Fragment() {
    val dataProcessor = DataProcessor.instance
    val patientsList = mutableListOf<PatientDto>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_patients, container, false)
        val textView: TextView = root.findViewById(R.id.text_patient_empty)
        patientsList.addAll(dataProcessor.getPatient())
        val patientsListView =  root.findViewById<ListView>(R.id.patientsListView)
        patientsListView.adapter = ArrayAdapter(this.context!!, android.R.layout.simple_list_item_1, patientsList.map { it.patientName })
        patientsListView.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this.context, PatientShow::class.java).apply {
                putExtra("patientId", patientsList[position].id.toString() ?: "0")
            }
            startActivity(intent)
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        patientsList.clear()
        patientsList.addAll(dataProcessor.getPatient())
        patientsListView.adapter = ArrayAdapter(this.context!!, android.R.layout.simple_list_item_1, patientsList.map { it.patientName })
    }
}
