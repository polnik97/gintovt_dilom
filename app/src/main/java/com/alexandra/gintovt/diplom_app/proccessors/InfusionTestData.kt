package com.alexandra.gintovt.diplom_app.proccessors

import android.hardware.Sensor
import com.alexandra.gintovt.diplom_app.dto.*
import java.time.LocalDateTime

val infusions = listOf(
    InfusionDto(
        1,
        1,
        "Раджах Фархид Валерьевич",
        1f,
        1,
        "Sensor 1",
        10f,
        "Drag",
        1000f,
        LocalDateTime.now(),
        LocalDateTime.now().plusHours(2),
        "Гинтовт Александра",
        "None"
    ),
    InfusionDto(
        2,
        2,
        "Мудживан Артур Протонович",
        1f,
        1,
        "Sensor 1",
        15f,
        "Drag",
        1500f,
        LocalDateTime.now(),
        LocalDateTime.now().plusHours(3),
        "Гинтовт Александра",
        "None"
    ),
    InfusionDto(
        3,
        3,
        "Саран Аропян Акопович",
        1f,
        1,
        "Sensor 1",
        7f,
        "Drag",
        1200f,
        LocalDateTime.now().minusHours(3),
        LocalDateTime.now().minusHours(1),
        "Гинтовт Александра",
        "None"
    ),
    InfusionDto(
        4,
        4,
        "Крапов Карпян Вонович",
        1f,
        1,
        "Sensor 1",
        7f,
        "Drag",
        1200f,
        LocalDateTime.now(),
        LocalDateTime.now().plusHours(3),
        "Гинтовт Александра",
        "None"
    )
)

val patients = listOf(
    PatientDto(
        1,
        "Раджах Фархид Валерьевич",
        1f
    ),
    PatientDto(
        2,
        "Мудживан Артур Протонович",
        2f
    ),
    PatientDto(
        3,
        "Саран Аропян Акопович",
        2f
    ),
    PatientDto(
        4,
        "Крапов Карпян Вонович",
        3f
    )
)

val drags = listOf(
    DragDto(
        1,
        "Флибуцин",
        1000f
    ),
    DragDto(
        1,
        "Физраствор",
        1000f
    ),
    DragDto(
        1,
        "Альбуцин",
        1000f
    )
)

val doctors = listOf(
    DoctorDto(
        1,
        "Гинтовт Александра",
        "Медсестра",
        "gintovt",
        "123"
    ),
    DoctorDto(
        2,
        "Полянский Николай Геннадьевич",
        "Доктор",
        "pol",
        "123"
    ),
    DoctorDto(
        3,
        "Хораськина Анна Олеговна",
        "Медсестра",
        "hor",
        "123"
    )
)

val sensors = listOf(
    SensorDto(
        1,
        "X-1",
        3f
    ),
    SensorDto(
        2,
        "X-2",
        3f
    ),
    SensorDto(
        3,
        "X-3",
        3f
    )
)