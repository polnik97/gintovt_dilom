package com.alexandra.gintovt.diplom_app.ui.currentInfusions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexandra.gintovt.diplom_app.R
import com.alexandra.gintovt.diplom_app.adapters.InfusionAdapter
import com.alexandra.gintovt.diplom_app.dto.InfusionDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import kotlinx.android.synthetic.main.current_infusions_fragment.*

class CurrentInfusionsFragment : Fragment() {
    var infusionAdapter : InfusionAdapter? = null
    private val dataProcessor = DataProcessor.instance
    private var currentInfusionsList = mutableListOf<InfusionDto>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.current_infusions_fragment, container, false)
        val textView: TextView = root.findViewById(R.id.text_current_infusions)
        val currentInfusionsRecycleView = root.findViewById<RecyclerView>(R.id.currentInfusionsRecycleView)
        if (dataProcessor.currentInfusionsIds.isEmpty()) textView.text = getString(R.string.empty_name) else textView.visibility = View.INVISIBLE
        currentInfusionsRecycleView.layoutManager = LinearLayoutManager(this.context)
        currentInfusionsList = mutableListOf<InfusionDto>()
        currentInfusionsList.addAll(dataProcessor.getInfusions().filter { dataProcessor.currentInfusionsIds.contains(it.id) }.sortedByDescending { it.endDate })
        infusionAdapter = InfusionAdapter(currentInfusionsList, this.context, this.activity)
        currentInfusionsRecycleView.adapter = infusionAdapter
        return root
    }

    override fun onResume() {
        super.onResume()
        currentInfusionsList.clear()
        currentInfusionsList.addAll(dataProcessor.getInfusions().filter { dataProcessor.currentInfusionsIds.contains(it.id) }.sortedByDescending { it.endDate })
        infusionAdapter?.updateAdapter(currentInfusionsList)
        currentInfusionsRecycleView.adapter = infusionAdapter
    }

}
