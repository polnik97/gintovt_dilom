package com.alexandra.gintovt.diplom_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import kotlinx.android.synthetic.main.activity_log_in.*

class LogIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
    }

    fun entireClick(view: View){
        val dataProcessor = DataProcessor.instance
        val doctors = dataProcessor.getDoctors()
        val doctorLogin =  loginValue.text
        val doctorPassword = password.text

        try {
            val correctUser =  doctors.first { it.login == doctorLogin.toString() }
            if (correctUser.passwords == doctorPassword.toString()){
                dataProcessor.currentUser = correctUser
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        } catch (e: NoSuchElementException){
            Toast.makeText(this, "Не правильный пользователь или пароль", Toast.LENGTH_SHORT).show()
        }
    }
}
