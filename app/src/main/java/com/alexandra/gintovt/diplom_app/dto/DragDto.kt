package com.alexandra.gintovt.diplom_app.dto

data class DragDto (
    val id: Int,
    val name: String,
    val value: Float
)