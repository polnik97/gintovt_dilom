package com.alexandra.gintovt.diplom_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.alexandra.gintovt.diplom_app.dto.InfusionDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import com.alexandra.gintovt.diplom_app.utils.formatToViewedTime
import kotlinx.android.synthetic.main.activity_infus_show.*

class InfusShow() : AppCompatActivity() {

    private val dataProcessor: DataProcessor = DataProcessor.instance
    private var infusionDto: InfusionDto? = null
    private var isTracked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_infus_show)
        val infusionDtoId = intent.getIntExtra("infusionId", 1)
        infusionDto = dataProcessor.getInfusionById(infusionDtoId)
        if(dataProcessor.currentInfusionsIds.contains(infusionDto!!.id)){
            isTracked = true
            stalckerButton.text = "Прекратить отслеживать"
            stalckerButton.textSize = 12f
        } else {
            stalckerButton.text = "Отслеживать"
            stalckerButton.textSize = 18f
        }
        patientName.text = infusionDto?.patientName
        patientRoom.text = infusionDto?.patientRoomNumber?.toInt().toString()
        dragName.text = infusionDto?.dragName
        dragValue.text = infusionDto?.dragValue.toString()
        sensorName.text = infusionDto?.sensorName
        sensorSpeed.text = infusionDto?.sensorSpeed.toString()
        startDate.text = infusionDto?.startDate?.formatToViewedTime() ?: ""
        endDate.text = infusionDto?.endDate?.formatToViewedTime() ?: ""
        createdBy.text = infusionDto?.creatorName
        lastChangeName.text = infusionDto?.lastChangerName
    }

    fun startStopStalk(view: View){
        if (infusionDto != null){
            if(isTracked){
                dataProcessor.currentInfusionsIds.remove(infusionDto!!.id)
                stalckerButton.text = "Отслеживать"
                stalckerButton.textSize = 18f
                isTracked = false
            }
            else{
                dataProcessor.currentInfusionsIds.add(infusionDto!!.id)
                stalckerButton.text = "Прекратить отслеживать"
                stalckerButton.textSize = 12f
                isTracked = true
            }
        }
    }
}
