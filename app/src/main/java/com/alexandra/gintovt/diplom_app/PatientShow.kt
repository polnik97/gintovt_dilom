package com.alexandra.gintovt.diplom_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SimpleAdapter
import com.alexandra.gintovt.diplom_app.dto.InfusionDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import com.alexandra.gintovt.diplom_app.utils.formatToViewedTime
import kotlinx.android.synthetic.main.activity_patient_show.*

class PatientShow : AppCompatActivity() {

    private val dataProcessor = DataProcessor.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_show)

        val currentPatient = dataProcessor.getPatientById(intent.getStringExtra("patientId").toInt())

        patientNameShow.text = currentPatient.patientName
        patientRoomShow.text = currentPatient.patientRoom.toInt().toString()

        val patientInfusions = dataProcessor.getInfusions().filter { it.patientId == currentPatient.id }
        val infusuinsArray = getMapForAdapter(patientInfusions)

        patientInfusionsListView.adapter = SimpleAdapter(this, infusuinsArray, android.R.layout.simple_list_item_2,
            listOf("dragName", "infusionDateStart").toTypedArray(),
            listOf(android.R.id.text1, android.R.id.text2).toIntArray()
        )
        patientInfusionsListView.setOnItemClickListener{ parent, view, position, id ->
            val intent = Intent(this, InfusShow::class.java).apply {
                putExtra("infusionId", patientInfusions[position].id)
            }
            startActivity(intent)
        }
    }

    private fun getMapForAdapter(infusionsList: List<InfusionDto>): List<Map<String, String>>{
        val array = mutableListOf<Map<String, String>>()
        for(item in infusionsList){
            val map = mutableMapOf<String, String>()
            map["dragName"] = item.dragName ?: ""
            map["infusionDateStart"] = item.startDate?.formatToViewedTime() ?: ""
            array.add(map)
        }
        return array
    }
}
