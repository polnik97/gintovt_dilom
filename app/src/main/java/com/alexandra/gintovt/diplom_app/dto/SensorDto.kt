package com.alexandra.gintovt.diplom_app.dto

data class SensorDto(
    val id: Int,
    val name: String,
    val speed: Float
)