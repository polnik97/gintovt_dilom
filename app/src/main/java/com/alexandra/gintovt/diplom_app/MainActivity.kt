package com.alexandra.gintovt.diplom_app

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.Menu
import android.view.View
import android.widget.ArrayAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.alexandra.gintovt.diplom_app.dto.PatientDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import kotlinx.android.synthetic.main.fragment_patients.*
import kotlinx.android.synthetic.main.nav_header_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val dataProcessor = DataProcessor.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        //getSupportActionBar()?.hide()

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        doctorNameHeader.text = dataProcessor.currentUser?.name
        doctorOccupationHeader.text = dataProcessor.currentUser?.occupation
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun onClickAddNew(view: View) {
        val intent = Intent(this, NewInfusion::class.java)
        this.startActivity(intent)
    }

    fun findPatient(view: View){
        val filterString = findPatient.text.toString()
        val patientsList = mutableListOf<PatientDto>()
        patientsList.addAll(dataProcessor.getPatient())
        val matchPatients = patientsList.filter { it.patientName.contains(filterString) }
        patientsListView.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, matchPatients.map { it.patientName })
    }
}
