package com.alexandra.gintovt.diplom_app.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


fun LocalDateTime.formatToViewedTime(): String {
    val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    return dateFormatter.format(this)
}