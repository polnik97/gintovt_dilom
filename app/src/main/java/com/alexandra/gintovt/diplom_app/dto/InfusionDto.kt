package com.alexandra.gintovt.diplom_app.dto

import java.time.LocalDateTime

data class InfusionDto(
    val id: Int,
    val patientId: Int?,
    val patientName: String?,
    val patientRoomNumber: Float?,
    val sensorId: Int?,
    val sensorName: String?,
    val sensorSpeed: Float,
    val dragName: String?,
    val dragValue: Float,
    val startDate: LocalDateTime?,
    val endDate: LocalDateTime?,
    val creatorName: String?,
    val lastChangerName: String?
)