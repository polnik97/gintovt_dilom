package com.alexandra.gintovt.diplom_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import com.alexandra.gintovt.diplom_app.dto.InfusionDto
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import com.alexandra.gintovt.diplom_app.ui.gallery.AllInfusionsFragment
import kotlinx.android.synthetic.main.activity_new_infusion.*
import java.time.LocalDateTime

class NewInfusion : AppCompatActivity() {

    private val dataProcessor = DataProcessor.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_infusion)

        val patientsAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProcessor.getPatient().map { it.patientName })
        patientsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        patientCh.adapter = patientsAdapter

        val dragsAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProcessor.getDrags().map { it.name })
        patientsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dragCh.adapter = dragsAdapter

        val sensorAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dataProcessor.getSensors().map { it.name })
        patientsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sensorCh.adapter = sensorAdapter
    }

    fun onClickSave(view: View){
        val patient = dataProcessor.getPatient().first{ it.patientName == patientCh.selectedItem.toString()}
        val drag = dataProcessor.getDrags().first{ it.name == dragCh.selectedItem.toString()}
        val sensor = dataProcessor.getSensors().first{ it.name == sensorCh.selectedItem.toString()}

        val infusion = InfusionDto(
            dataProcessor.getInfusions().size.plus(1),
            patient.id,
            patient.patientName,
            patient.patientRoom,
            sensor.id,
            sensor.name,
            sensor.speed,
            drag.name,
            dragValue.text.toString().toFloat(),
            LocalDateTime.now(),
            LocalDateTime.now().plusSeconds(dragValue.text.toString().toFloat().div(sensor.speed).toLong()),
            "Гинтовт Александра",
            "None"
        )
        dataProcessor.saveInfusion(infusion)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
