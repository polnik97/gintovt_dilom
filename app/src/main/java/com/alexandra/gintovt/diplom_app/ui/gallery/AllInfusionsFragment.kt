package com.alexandra.gintovt.diplom_app.ui.gallery

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexandra.gintovt.diplom_app.NewInfusion
import com.alexandra.gintovt.diplom_app.R
import com.alexandra.gintovt.diplom_app.adapters.InfusionAdapter
import com.alexandra.gintovt.diplom_app.proccessors.DataProcessor
import com.alexandra.gintovt.diplom_app.proccessors.infusions
import kotlinx.android.synthetic.main.all_infusions.*

class AllInfusionsFragment : Fragment() {
    var infusionAdapter : InfusionAdapter? = null
    val dataProcessor = DataProcessor.instance

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.all_infusions, container, false)
        val infusionsRecyclerView = root.findViewById<RecyclerView>(R.id.infusionsRecyclerView)
        val textView: TextView = root.findViewById(R.id.text_infusions)
        if (dataProcessor.getInfusions().isEmpty()) textView.text = getString(R.string.empty_name) else textView.visibility = View.INVISIBLE
        infusionsRecyclerView.layoutManager = LinearLayoutManager(this.context)
        infusionAdapter = InfusionAdapter(dataProcessor.getInfusions().sortedByDescending { it.endDate }, this.context, this.activity)
        infusionsRecyclerView.adapter = infusionAdapter
        return root
    }

    fun onClickAddNew() {
        val intent = Intent(context, NewInfusion::class.java)
        context?.startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        infusionAdapter?.updateAdapter(dataProcessor.getInfusions().sortedByDescending { it.endDate })
        infusionsRecyclerView.adapter = infusionAdapter
    }


}
