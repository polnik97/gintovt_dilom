package com.alexandra.gintovt.diplom_app.dto

data class PatientDto (
    val id : Int,
    val patientName: String,
    val patientRoom: Float
)