package com.alexandra.gintovt.diplom_app.dto

data class DoctorDto (
    val id: Int,
    val name: String,
    val occupation: String,
    val login: String,
    val passwords: String
)